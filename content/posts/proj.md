---
title: "Preda 2020 Projects"
date: 2020-12-27T05:44:27+08:00
draft: false
tags: [final report, presentation]
---

![](https://assets.phillips.com/image/upload/t_Website_LotDetailMainImage/v1/auctions/NY010414/211_001.jpg) 
Alexander Calder Composition (Pyramids and Sun on Target) from [Phillips](https://www.phillips.com/detail/ALEXANDER-CALDER/NY010414/211?fromSearch=calder&searchPage=1)


### 期末報告

- 資料書推。推資料書。。。：選書/報告 (report , thesis or paper) 原則
- 課堂presentation: 一組 **20～25** 分鐘。每位組員都要上台為原則，整合整組內容，而非截斷假分工。 一堂50分鐘課程有2組報告，中間休息3分鐘。
- 書面報告，內容為推介本次報告之主題，非投影片列印。
- NEW! 為讓報告同學有更完整的舞台體驗，設置 20pDM 觸發機制：當聽眾人數（不含Kno 及報告小組成員）$\leq 20$ 人時，得由 Kno 或 出席同學提出人數清點點名，或照相存證。教師得以該點名出席調整學期成績。本機制於2020 1221 經同學認可，1224起實施。

### 重要日期

- 12/21(M) 開始報告 （報告日期：12/24; 12/28, 12/31, 1/4, 1/7）
- 2021 01/17(Sun) 23:59   書面報告(pdf preferred)繳交截止, 以 email 寄送日期計算 。並請註明是否願意公開報告投影片及書面報告。

### 報告順序
依照課堂透過當天同學隨機產生起始值及 R sample 產生順序如下 [2  1 18 17  7 13, 10 16  9 15 12  4,  6  5  8 11
3 14]。

正式報告標題/主題請於報告前一星期 email 給我。

* 12/21(一)
  * 2 運動飲料：許廷綸, 陳宥睿。[連結]()
* 12/24 (四)
  * 18 LOL: 龔佑倫, 魏世學, 邱威誠
  * 17 Apex Legends: 張聖岳, 謝宜晴
  * 7 機車: 楊祥弘, 盧彥希, 賴冠廷
  * 13 德州撲克: 陳冠綸, 張祐華
* 12/28 (一)
  * 10 武漢肺炎對台灣的影響: 詹淩棋
  * 16 青少年運動定位app使用調查分析: 康瑞君, 吳家伶
* 12/31 (四)
  * 1 論嚴重特殊傳染性肺炎(COVID-19)對全球電影業的影響：李永仁, 王弘奇 [連結](): 
  * 15 十二星座大亂鬥: [12 星座小探員] 吳紀煌, 鍾宜玲
  * 12 特戰英雄玩什麼？: 林子齊, 劉承勛
  * 4 淺析台韓娛樂產業的差別: 吳沁璇, 華詩詠
* 1/4 (一)
  * 6 憂鬱症好發年齡與性別比: 吳宛臻, 吳聿璿
  * 5 摺耳貓可愛背後的痛: 蔡念秤, 王佑淳
* 1/7 (四)
  * 8 洗髮精的選擇: 江庭育
  * 11 哪家珍珠紅茶拿鐵受歡迎?: 朱晏鞍, 賴雅婷
  * 3 性侵害殺人犯罪之研究: 蔡以宣, 梁正擷, 何秀麗
  * 14 交通事故分析: 鄭文愷


### 說明
* 準備報告時可以隨時問問自己以下問題
	* 這本書/論文主要關心的問題
	* 聚焦的問題/圖表/統計數字
	* 針對聚焦問題的心得，文獻資訊探索
	* 參考資料與延伸閱讀
* 書面報告內容與結構建議
	* 摘要: 總結本報告內容
	* 介紹: 為何這本書/論文的內容為何重要/為何是你該知道的？它會對你有何幫助/成長？
	* 主要內容
	* 結論/Take-home message
	* 參考資料/延伸閱讀/連結網址
	
#### 連結

* [Story structure – the hidden framework that hangs your story together](https://www.presentation-guru.com/on-structure-the-hidden-framework-that-hangs-your-story-together/)
* [千分之一約等於十分之七的二十次方](https://www.youtube.com/watch?v=1Nub7szsLow)
* [What's Wrong with TED Talks? Benjamin Bratton at TEDxSanDiego 2013](https://youtu.be/Yo5cKRmJaf0)

#### 你可以學得更好, 更開心, 更有效率 
其他科目都可以不及格，只有 學習如何學習 絕對要學會。在現在這個時代更是如此。但與其說它是一個要修的科目，不如把它想成一種探索未知世界的態度。以下是最近看到一篇寫得相當好而精簡的部落格文。探討學習有不少經典的書籍。可貴的是，這篇文章精要地總結了許多現代關於學習的新理論與角度。以Kno~~一個大學成績不太好，但後來還學得蠻開心，在這領域多年依然樂在其中＋自學一些有的沒有的還能自得其樂的人~~的視角來看, 要是能早點在大學知道這些，應該可以學得更好，更開心也更有效率。我強烈推薦與分享

[The only technique to learn something new](https://boingboing.net/2015/05/11/the-only-technique-to-learn-so.html) ([James Altucher](https://boingboing.net/2016/04/15/how-minimalism-brought-me-free.html) @Boing Boing)

**學會任何新事物的唯一技法**。作者將它分為十個步驟，由愛她開始。文不算短，你可以先瀏覽大標題：1. Love It, 2. Read it. 3. Try it, but not too hard, ... 你沒看錯，由愛她開始。

加映一個相關的 quote
> Wisdom is a love affair with questions. Knowledge is a love affair with answers. 
> -- <cite>Julio Olalla</cite>

##### 延伸閱讀
* [動機，單純的力量](https://www.books.com.tw/products/0010476180) (Daniel Pink). 紅蘿菠與棍子不僅無法驅動稍有創意的行為，甚至有反效果。
* [刻意練習：比天賦更關鍵的學習法](https://www.books.com.tw/products/0010752714?loc=P_asb_006)(Anders Ericsson and  Robert Pool); 這是「一萬個小時」的原始研究，相較於[異數:成功的故事](https://www.books.com.tw/products/0010668300)(Malcolm Gladwell),  Ericsson and Pool 有更深入而完整的討論--練習的時間長度不是一個領域佼佼者成功的原因，主動、專一、而有計畫步驟的刻意練習才是。
* [心流：最優體驗心理學](https://www.books.com.tw/products/0010816703)(Mihaly Csikszentmihalyi)
[![](https://miro.medium.com/max/362/1*ShU0zwZKC1ti_Oc2f86nPA.png)](https://uxdesign.cc/7-steps-to-achieving-flow-in-ux-design-7ef28adb0de2)

