---
title: "Week 10.1 Data Vis: Map"
date: 2020-11-16T20:56:10+08:00
draft: false
tags: [map, Beck map, flavour network, game board design]
---

![](https://assets.londonist.com/uploads/2016/05/i875/1931.jpg)

### Map

在英文中 map 不僅指地圖，而有更廣泛的意涵，如[歷史事件圖](https://en.wikipedia.org/wiki/A_New_Chart_of_History)，[演化圖](https://pokemondb.net/evolution)。更適合的理解，map $\approx$ 一群物件以及其關聯的圖形/圖表呈現。Beck map 可以說是一個非常具代表性的例子。
1. Beck map: [The history of the tube map @ Londonist](https://londonist.com/2016/05/the-history-of-the-tube-map),  [The genious of Harry Beck ... @ OpenCulture](http://www.openculture.com/2018/04/the-genius-of-harry-becks-1933-london-tube-map.html) Design principles of good UI: Focus, Simplicity, Thinking cross-disciplinarily
2. [資料視覺化設計的潮流 永原康史](https://www.books.com.tw/products/0010787738)
3. 一個 map 是好或壞，適當與否，必須先問幾個問題：它關心的重點在哪裡？物件是哪些？哪些是要刻劃的首要關係？優先順序與取捨
4. Flavour Network:  Flavor network and the principles of food pairing [@ Nature](https://www.nature.com/articles/srep00196) [MIT Tech Review](https://www.technologyreview.com/2011/11/29/189470/flavour-networks-shatter-food-pairing-hypothesis/), [Flavor and Unami Network](https://foodgalaxy.jp/FlavorNetwork/), 
5. 一個桌游的主板塊模組、記分板、資源板等也可以這些準則來評估與設計，主要訊息與次要資料要有主從，但必要時也可查詢。每輪小結時資源或分數的計算，要清晰易懂，也讓其他玩家一目了然。說來簡單，但是並不容易做到， [Clans of Caledonia](https://boardgamegeek.com/boardgame/216132/clans-caledonia), [Scythe](https://boardgamegeek.com/boardgame/169786/scythe) 我認為是其中佼佼者。

Data Visualization 與 map 的想法相通--沒有清楚的目標架構、優先順序與取捨，就沒有好的 data vis. 