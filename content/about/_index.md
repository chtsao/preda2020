---
title: About
date: 2020-09-13 16:31:39
---
[![Frédéric Chopin - Prelude in E-Minor (op.28 no. 4)](https://musescore.com/static/musescore/scoredata/gen/4/7/4/65474/3010386e746630050b868ecb0e2824dd756ef383/score_0.svg?no-cache=1567426587)](https://youtu.be/ef-4Bv5Ng0w)
Prelude to Data Analysis 2020 Course Web
[Course Syllabus](http://faculty.ndhu.edu.tw/~chtsao/edu/20/preda/preda2020syllabus.pdf)
Curator: Kno Tsao